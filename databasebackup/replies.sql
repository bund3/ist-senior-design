-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2017 at 04:08 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `istsrdesign`
--

-- --------------------------------------------------------

--
-- Table structure for table `replies`
--

CREATE TABLE `replies` (
  `replyid` int(11) NOT NULL,
  `threadid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `replies`
--

INSERT INTO `replies` (`replyid`, `threadid`, `userid`, `timestamp`, `content`) VALUES
(1, 1, 2, '2016-10-24 20:46:01', 'You can us the import statement to add one class to another. Make sure the path to the class is correct.'),
(2, 1, 1, '2016-10-24 20:46:01', 'I have tried that already, it\'s still not working. Any idea?'),
(3, 2, 2, '2016-10-25 19:48:12', 'I can help, When are you available?'),
(4, 2, 2, '2016-10-27 02:02:26', 'At 4 o\'clock today in CSC. THanks!'),
(21, 2, 1, '2016-11-16 01:55:28', 'another one'),
(24, 2, 1, '2016-11-17 23:54:31', 'anotherone'),
(25, 4, 1, '2016-11-18 00:28:33', 'And I am replying to it'),
(26, 4, 1, '2016-11-18 00:28:45', 'Oh, it works!'),
(27, 3, 1, '2016-11-18 00:29:02', 'Let\'s try this onw'),
(29, 6, 1, '2016-11-18 00:29:45', 'Oh yes you got to bro'),
(32, 1, 1, '2016-11-18 18:42:46', 'lkjsdjfd'),
(33, 1, 1, '2016-11-18 18:43:13', 'Testing a reply'),
(34, 1, 1, '2016-11-18 18:44:00', 'kjhjdhs'),
(35, 4, 1, '2016-11-18 18:44:22', 'ksjdjhfdjf'),
(36, 6, 1, '2016-11-18 18:45:29', 'Haha'),
(37, 1, 1, '2016-11-18 20:00:56', 'dsfdfdf'),
(38, 2, 1, '2016-11-18 20:01:03', 'dfdfdf'),
(39, 1, 1, '2016-11-19 21:29:43', 'ddffd'),
(40, 1, 1, '2016-11-19 21:31:39', 'sdsds'),
(41, 1, 1, '2016-11-19 21:51:33', 'dklfjkdf'),
(42, 1, 1, '2016-11-19 21:51:38', 'ldkjfkd'),
(43, 1, 1, '2016-11-19 21:55:37', 'dfd'),
(44, 1, 1, '2016-11-19 21:58:14', 'dsffds'),
(45, 1, 1, '2016-11-21 05:24:10', 'animate'),
(46, 2, 1, '2016-11-21 05:24:18', 'NIMrw'),
(47, 2, 1, '2016-11-25 17:51:44', 'dlkje dh4eiorf e'),
(48, 1, 1, '2016-11-25 17:55:38', 'dlfkjdkfjd'),
(49, 2, 1, '2016-11-25 17:56:23', 'lksdjkjdksfj'),
(50, 3, 1, '2016-12-02 18:19:12', 'this is a test reply to this post!'),
(51, 5, 1, '2016-12-02 18:20:25', 'what\'s going on?'),
(52, 10, 1, '2016-12-05 04:11:42', 'oiuykjuih'),
(53, 12, 1, '2016-12-07 23:31:07', 'thdkehjd'),
(54, 12, 1, '2016-12-07 23:31:13', 'lkjsdkjdf'),
(55, 12, 1, '2016-12-07 23:31:18', 'lkjdskjdsf'),
(56, 12, 1, '2016-12-07 23:31:21', 'sxlkjdsfjdsf'),
(57, 13, 1, '2016-12-07 23:31:35', ';lskdlkfjsdkfsd'),
(58, 2, 1, '2016-12-09 05:49:54', '3e3430409dsfdsf'),
(59, 3, 1, '2016-12-09 06:08:55', 'sdfkhfdjsfhjdfh'),
(60, 14, 1, '2016-12-11 18:53:44', 'this is a test reply to this post!');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `replies`
--
ALTER TABLE `replies`
  ADD PRIMARY KEY (`replyid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `replies`
--
ALTER TABLE `replies`
  MODIFY `replyid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
