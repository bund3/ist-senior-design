-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2017 at 04:06 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `istsrdesign`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userid` int(4) NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `muemail` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `datecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `username`, `firstname`, `lastname`, `password`, `admin`, `active`, `muemail`, `datecreated`) VALUES
(1, 'bunde', 'Fabunde', 'Mamey', 'password', 0, 1, '10936273', '2016-10-24 07:00:00'),
(2, 'Zuba', 'Joseph', 'Kollie', 'password', 0, 1, '10936274', '2016-10-24 07:00:00'),
(5, 'joeb', 'Joe', 'Blo', 'pass', 0, 1, '10970626@live.mercer.edu', '2016-11-17 22:29:10'),
(19, 'bunde1', 'Fabunde', 'Mamey', 'pass', 0, 1, 'fabunde15@live.mercer.edu', '2016-12-09 07:11:52'),
(21, 'bunde13', 'Fabunde', 'Mamey', 'pass', 0, 1, 'fabunde15@live.mercer.edu', '2016-12-09 07:11:57'),
(23, 'kai', 'Fabunde', 'Mamey', 'pass', 0, 1, 'fabunde15@gmail.com', '2016-12-09 07:18:36'),
(24, 'mass', 'Fabunde', 'Mamey', 'pass', 0, 1, 'fabunde15@live.mercer.edu', '2016-12-09 07:21:56'),
(25, 'zoe1', 'Fabunde', 'Mamey', 'pass', 0, 1, 'fabunde15@live.mercer.edu', '2016-12-09 07:23:29'),
(26, 'skldjf', 'Fabunde', 'Mamey', 'pass', 0, 1, 'fabunde15@live.mercer.edu', '2016-12-09 14:06:19'),
(27, 'dlskjkfj', 'Fabunde', 'Mamey', 'pass', 0, 1, 'fabunde15@live.mercer.edu', '2016-12-09 14:15:49'),
(28, 'sdfjdf', 'Fabunde', 'Mamey', 'pass', 0, 1, 'fabunde15@live.mercer.edu', '2016-12-09 14:18:28'),
(29, 'djkdsdkl', 'Fabunde', 'Mamey', 'pass', 0, 1, 'fabunde15@live.mercer.edu', '2016-12-09 14:24:12'),
(30, 'test1', 'Zoe', 'becton', 'pass', 0, 1, '10970626@live.mercer.edu', '2016-12-09 14:56:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userid`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userid` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
