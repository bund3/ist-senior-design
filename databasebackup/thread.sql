-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2017 at 04:06 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `istsrdesign`
--

-- --------------------------------------------------------

--
-- Table structure for table `thread`
--

CREATE TABLE `thread` (
  `threadid` int(6) NOT NULL,
  `userid` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `thread`
--

INSERT INTO `thread` (`threadid`, `userid`, `timestamp`, `content`) VALUES
(1, 1, '2016-10-24 20:43:12', 'Does anyone know how to make a class available to another in Java?'),
(2, 2, '2016-10-24 20:43:12', 'I am working on my site layout but can\'t get it to work. Can someone please help? I\'m using bootstrap.'),
(3, 1, '2016-12-02 18:17:32', 'This is a test post'),
(4, 1, '2016-12-02 18:19:35', 'testing testing'),
(5, 1, '2016-12-02 18:20:10', 'another test'),
(6, 1, '2016-12-02 18:20:39', 'test test test test test'),
(7, 1, '2016-12-02 18:20:51', 'test test test test testkjhdfkdsfhdsjkfds'),
(8, 1, '2016-12-02 18:21:02', 'khdskhfdslkjflhdsfkjhdsfjkldsf'),
(9, 1, '2016-12-02 18:21:12', 'hkdshfjhdsjfh29e09303o4jlkle'),
(10, 1, '2016-12-02 18:21:27', 'test test test'),
(11, 1, '2016-12-05 04:11:49', 'khjhgjhgjhgh'),
(12, 1, '2016-12-05 04:11:58', 'khjhgjhgjhghjhjklkjklj'),
(13, 1, '2016-12-07 23:31:30', 'l;kxlkdslkjdsj sdlkjgjkdg'),
(14, 1, '2016-12-09 06:09:02', 'lkjdsfsfhkdshfkjhsdjkfhdsf'),
(15, 1, '2016-12-09 06:17:20', 'angular is great!');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `thread`
--
ALTER TABLE `thread`
  ADD PRIMARY KEY (`threadid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `thread`
--
ALTER TABLE `thread`
  MODIFY `threadid` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
