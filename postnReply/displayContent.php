<?PHP
require('../core/connection.php');
/*-------------------retrieving data------------------*/
/*$data = json_decode(file_get_contents("php://input"));
$threadReply = mysqli_real_escape_string($conn, $data->reply);
$password =  mysqli_real_escape_string($conn, $data->id);*/
	 
//getdata form db
$query = "SELECT users.username, users.userid, thread.threadid, thread.content, thread.timestamp FROM users JOIN thread ON users.userid=thread.userid";

/*"SELECT users.username, thread.content, thread.userid FROM users JOIN thread ON users.userid=thread.userid";*/


$selectSql = mysqli_query($conn, $query);
if(!$selectSql){
	die("Selection failed");
} 


$postnReplies = [];
// $repliesArray = [];
while ($row = mysqli_fetch_assoc($selectSql)) {	

	$postnReplies[] = $row;

}

$repliesQuery = "";

for ($i=0; $i < sizeof($postnReplies); $i++) {
	$user = $postnReplies[$i]['username'];
	$post = $postnReplies[$i]['content'];
	$threadId = intval($postnReplies[$i]['threadid']);

	// echo $threadId;

	$repliesQuery = "SELECT replies.content, replies.userid FROM replies JOIN thread ON replies.threadid=thread.threadid WHERE replies.threadid=" . $threadId; /*thread.threadid*/

	$reSelectSql = mysqli_query($conn, $repliesQuery);
	if(!$reSelectSql){
		die("Replies selection failed");
	} 


	$repliesResult = [];
	while ($row = mysqli_fetch_assoc($reSelectSql)) {
		$repliesResult[] = $row;
	}
	$postnReplies[$i]['replies'] = $repliesResult;
} //end of for loop


echo json_encode($postnReplies);
/*-------------------retrieving data ends------------------*/

?>

<?php mysqli_close($conn);	?>
	