<!DOCTYPE html>
<html lang="en" ng-app="messageBoard">
<head>
<title>Senior Design Project</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script> -->
<style>
.active{
    color: red;
}
</style>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="styles.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.3.1/angular-ui-router.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ngStorage/0.3.6/ngStorage.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular-animate.min.js"></script>

<body> <!-- ng-controller="rootCntrl" -->
        <nav class="navbar navbar-default" ng-controller="navCtrl">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                <a class="navbar-brand" href="#">C.S. MessagingBoard</a>
            </div>

            <!-- <div class="container-fluid "> -->
                <div class="collapse navbar-collapse" id="navbar">   
                <!-- {{test}}          -->
                    <ul class="nav navbar-nav navbar-right">
                        <li ng-if="$storage.user.username"><a id="userName" href="#">Hello {{$storage.user.username}}!</a></li>
                        <li><a ui-sref="threads" ui-sref-active="active">Home</a></li>
                        <li ng-if="!$storage.user.username">
                            <a href="#" ui-sref="login" ui-sref-active="active"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
                        <li ng-if="$storage.user.username"><a href="#" ng-click="logOut()"> <span class="glyphicon glyphicon-log-out"></span>Log out</a></li> 
                        <li ng-if="!$storage.user.username"> <a ui-sref="signup" ui-sref-active="active"><span class="glyphicon glyphicon-user"></span>Sign up </a></li>
                        <!-- <li ng-show="showUserGreet"><a href="#">Hello {{currentUser.username}}</a></li>
                        <li><a ui-sref="threads" ui-sref-active="active">Home</a></li>
                        <li ng-show="showLogin">
                            <a href="#" ui-sref="login" ui-sref-active="active"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
                        <li ng-show="showlogout" ><a href="#" ng-click="logOut()">Log out</a></li> 
                        <li ng-show="showSignup"> <a ui-sref="signup" ui-sref-active="active"><span class="glyphicon glyphicon-user"></span>Sign up </a></li> -->
                    </ul>
                </div>
            <!-- </div> -->
        </nav>
        <div class="container-fluid" ui-view>

        </div>
    	<!-- <p>{{userName}} and pass: {{password}}</p> -->

    <!-- <div ng-controller="displayPost">
        Search: <input ng-model="searchTerm">
        <ul>
        	{{name}}
        	<li ng-repeat="x in allPosts | filter:searchTerm">
        		<div>{{x.content}} </div>

        		<div>{{x.username}}</div>
        		<ul>
        			<li ng-repeat="y in x.replies">
        				<div> {{y.content}}</div>
        			</li>
        		</ul>	
        	</li>
        </ul>
    </div> -->

<script src="app.js"></script>
<script src="login/loginController.js"></script>
<script src="postnReply/threadController.js"></script>
<script src="signup/signUpController.js"></script>
<script src="core/service.js"></script>
<script src="mainCtrl.js"></script>


</body>
</html>

                            
                            
                            
                            
                            