app.controller("loginController",  function($scope, $rootScope, $http, userData, $localStorage, $state){

	$rootScope.$storage = $localStorage;	
	$rootScope.$storage.user = null;

	$scope.loginInfo = {
		username: '',
		password: ''
	}

	$scope.loginFail = false;
	$scope.loginUser = function(data){
		//var url = "login/login.php";
		
		var data = {
			username: $scope.loginInfo.username,
			password: $scope.loginInfo.password
		}

		userData.postToServer("login/login.php", data, function(response){
			if(typeof(response)  !== "string"){
				$rootScope.$storage.user = response;
				/*$scope.$apply(function(){
						$rootScope.$storage.user = response;
					});*/

				/*function(currentUser){
						$rootScope.$storage.user = currentUser;
					}*/
				// console.log("Storage" + $rootScope.$storage.user);
				$state.go('threads');
				$rootScope.$emit('userChange', $rootScope.$storage.user);
				// $window.location.assign('/istseniordesign/#/');
				//notify thead controller of user change
			}else{
				console.log("isfailed");
				$scope.loginFail = true;
				// console.log("service: " + userData.loginUser(data));				
			}
		});

	}//$scope.loginUser ends
})