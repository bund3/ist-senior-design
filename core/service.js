/*app.factory('userData', ['$http', function($http){
	// var url = './login/login.php';
	var userData = {};

	userData.loginUser = function(data){
		var url = "login/login.php";
		var result  = undefined;
		return $http.post(url, data);

	}


	return userData;

}]);*/

app.factory('userData', ['$http', function($http){

	return {
		postToServer: function(url, data, callback){
		$http.post(url, data).success(function(response){
			callback(response);
		})

	},

		/*getData: function(callback){
			var url = 'postnReply/displayContent.php';
			$http.get(url).then( function success(response){
				callback(response);
			}, function error(response){
				console.log(response.statusText);
			})
		}*/

		getData: function(){
			var url = 'postnReply/displayContent.php';
			return $http.get(url).then( function(response){
				// callback(response);
				return response.data;
				/*console.log(response.data);
				$scope.allPosts = response.data;*/
			});
	}

}

}]);
