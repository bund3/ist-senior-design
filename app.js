var app = angular.module('messageBoard', ['ui.router', 'ngStorage', 'ngAnimate']);

/*app.service('getRepliesnPost', function($http){

	var url = 'postnReply/displayContent.php';
	this.theResult = null;
	$http.get(url).then( function success(response){
		console.log(response.data);
		this.theResult = function(){ return response.data };
	}, function error(response){
		console.log(response.statusText);
	})
});*/

/*app.service('getRepliesnPost', function($http){

	// this.theResult = null;
	var url = 'postnReply/displayContent.php';


// this.allPosts = null;
var theresults = {};
	this.theResult = function(){
		$http.get(url).then( function success(response){
		// console.log(response.data);
		theresults = response.data;
	}, function error(response){
		console.log(response.statusText);
	})//"testing!!";	
		
		return theresults;
	}
});*/


app.config(function($stateProvider, $urlRouterProvider){

	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state("threads", {
			url:"/",
			controller: "threadController",
			templateUrl: "postnReply/threads.html"
		})

		.state("login", {
			url:"/login",
			controller: "loginController",
			templateUrl: "login/login.html"
		})

		.state("signup", {
			url: "/signup",
			controller: "sigUpController",
			templateUrl: "signup/signup.html"
		})
});

/*app.controller("rootCntrl", function($scope, $stateProvider){
	$scope.gotoLogin = function(){
	console.log("here");
	$state.go("login");
}

})
*/


// isLogin = false;
/*app.controller('displayPost', function($scope, $http) {
    // $scope.name = "John Doe";
    $http.get('endpoints/displayContent.php').then(function (response){
    	$scope.allPosts = response.data;
    });*/


/*app.controller('logInContrl', function($scope){
	$scope.userName = "bunde";
});*/